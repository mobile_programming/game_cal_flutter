// main.dart
import 'package:flutter/material.dart';
import 'package:game_cal_flutter/Level/level_divide.dart';
import 'package:game_cal_flutter/Level/level_minus.dart';
import 'package:game_cal_flutter/Level/level_multiply.dart';
import 'package:game_cal_flutter/Level/level_plus.dart';
import 'package:game_cal_flutter/Plus/plus1.dart';
import 'package:game_cal_flutter/view/mode.dart';
import '../Start.dart';

class End extends StatelessWidget {
  const End({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 97, 214, 246),
      body: Stack(
        children: <Widget>[
          Center(
            child: Column(children: <Widget>[
              SizedBox(
                height: 40,
              ),
              Image(
                image: AssetImage("img/Balloon2.png"),
                width: 300,
                height: 350,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  "GOOD JOB",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ]),
              SizedBox(
                height: 10,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  "YOU DID IT!!!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.yellow),
                ),
              ]),
              SizedBox(
                height: 10,
              ),
              Image(
                image: AssetImage("img/dogg.png"),
                width: 90,
                height: 140,
              ),
              SizedBox(
                height: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Opacity(
                      opacity: 1,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(300, 80),
                          primary: Color.fromARGB(255, 158, 222, 128),
                        ),
                        child: Text(
                          "PLAY AGAIN",
                          style: TextStyle(fontSize: 30, color: Color.fromARGB(255, 253, 253, 253),fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Mode()),
                          );
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Opacity(
                      opacity: 1,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(300, 80),
                          primary: Color.fromARGB(255, 224, 80, 80),
                        ),
                        child: Text(
                          " END GAME ",
                          style: TextStyle(fontSize: 30, color: Color.fromARGB(255, 236, 234, 234),fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Start()),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
