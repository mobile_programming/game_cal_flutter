// main.dart
import 'package:flutter/material.dart';
import 'package:game_cal_flutter/Minus/minus1.dart';
import 'package:game_cal_flutter/Minus/minus2.dart';
import 'package:game_cal_flutter/Minus/minus3.dart';

import '../main.dart';
import '../view/mode.dart';

class LevelMinus extends StatelessWidget {
  const LevelMinus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Level',
      home: Scaffold(
        body: const Button(),
      ),
    );
  }
}

class Button extends StatefulWidget {
  const Button({super.key});

  @override
  State<Button> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  @override
  Widget build(BuildContext context) => Opacity(
        opacity: 0.5,
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/Balloon.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Stack(
              children: [
                //back
                Align(
                  alignment: Alignment(-0.93, -0.4),
                  child: Container(
                    // padding: EdgeInsets.all(152),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(90, 70),
                          primary: Colors.black87,
                        ),
                        child: const Icon(
                          Icons.arrow_back_sharp,
                          size: 50,
                        ),
                        onPressed: () {
                          //Action to previous page
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Mode()),
                          );
                        },
                      ),
                    ),
                  ),
                ),

                //Beginner
                Align(
                  alignment: Alignment(0.0, -0.15),
                  child: Container(
                    // padding: EdgeInsets.all(152),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(375, 80),
                          textStyle: TextStyle(fontSize: 24),
                          primary: Color.fromARGB(255, 0, 139, 246),
                        ),
                        child: Text("BEGINNER"),
                        onPressed: () {
                          //Action to Beginner Level
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Minus()),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                //Medium
                Align(
                  alignment: Alignment(0.0, 0.1),
                  child: Container(
                    // padding: EdgeInsets.all(152),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(375, 80),
                          textStyle: TextStyle(fontSize: 24),
                          primary: Color.fromARGB(255, 0, 139, 246),
                        ),
                        child: Text("MEDIUM"),
                        onPressed: () {
                          //Action to Medium Level
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Minus2()),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                //Advanced
                Align(
                  alignment: Alignment(0.0, 0.35),
                  child: Container(
                    // padding: EdgeInsets.all(152),
                    child: Container(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(375, 80),
                          textStyle: TextStyle(fontSize: 24),
                          primary: Color.fromARGB(255, 0, 139, 246),
                        ),
                        child: Text("ADVANCED"),
                        onPressed: () {
                          //Action to Advanced Level
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Minus3()),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
