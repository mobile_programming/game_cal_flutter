// main.dart
import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:game_cal_flutter/End.dart';
import 'package:game_cal_flutter/view/mode.dart';
import 'package:game_cal_flutter/Start.dart';
// import our custom number keyboard
import '../keyboard_num_pad.dart';

class Mutiply extends StatelessWidget {
  const Mutiply({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  //time
  late AnimationController controller;

  bool isPlaying = false;

  String get countText {
    Duration count = controller.duration! * controller.value;
    return controller.isDismissed
        ? '${(controller.duration!.inMinutes % 60).toString().padLeft(2, '0')}:${(controller.duration!.inSeconds % 60).toString().padLeft(2, '0')}'
        : '${(count.inMinutes % 60).toString().padLeft(2, '0')}:${(count.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  double progress = 1.0;

  void notify() {
    if (countText == '00:00') {
      //time out and go to Start
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const End()));
    }
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      //set Time 300
      duration: const Duration(seconds: 300),
    );

    controller.addListener(() {
      notify();
      if (controller.isAnimating) {
        setState(() {
          progress = controller.value;
        });
      } else {
        setState(() {
          progress = 1.0;
          isPlaying = false;
        });
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  // text controller
  static const maxSeconds = 60;
  int count = maxSeconds;
  final TextEditingController _myController = TextEditingController();

  int randomNum1 = 1;
  int randomNum2 = 1;
  int sum = 0;
  int point = 0;
  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: [
              //time
              Expanded(
                child: Stack(
                  children: [
                    GestureDetector(
                      child: AnimatedBuilder(
                        animation: controller,
                        builder: (context, child) => Text(
                          countText,
                          style: const TextStyle(
                            fontSize: 34,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          if (controller.isAnimating) {
                            controller.stop();
                            setState(() {
                              isPlaying = false;
                            });
                          }
                        },
                        child: Container()),
                  ],
                ),
              ),
              Container(
                child: ElevatedButton(
                  child: const Icon(Icons.pause),
                  onPressed: () {
                    //pause time
                    if (controller.isAnimating) {
                      controller.stop();
                      setState(() {
                        isPlaying = false;
                      });
                    }
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(actions: [
                            const SizedBox(
                              height: 20,
                            ),
                            const Center(
                              child: Text("PAUSED",
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  )),
                            ),
                            //Resume
                            const SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Opacity(
                                opacity: 0.89,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: const Size(275, 80),
                                    primary: Colors.lightBlueAccent,
                                  ),
                                  child: const Icon(Icons.play_arrow_outlined,
                                      size: 40),
                                  onPressed: () {
                                    //Continue time
                                    controller.reverse(
                                        from: controller.value == 0
                                            ? 1.0
                                            : controller.value);
                                    setState(() {
                                      isPlaying = true;
                                    });
                                    //hide dialog
                                    Navigator.of(context, rootNavigator: true)
                                        .pop();
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Opacity(
                                opacity: 0.89,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: const Size(275, 80),
                                    primary:
                                        const Color.fromARGB(255, 91, 183, 104),
                                  ),
                                  child: const Icon(
                                    Icons.loop,
                                    size: 40,
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Mode()),
                                    );
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Opacity(
                                opacity: 0.89,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: const Size(275, 80),
                                    primary: const Color.fromARGB(
                                        255, 233, 104, 104),
                                  ),
                                  child:
                                      const Icon(Icons.output_sharp, size: 40),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Start()),
                                    );
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ]);
                        });
                  },
                ),
              ),
              //point
              const Padding(padding: EdgeInsets.all(48)),
              Row(
                children: [
                  Text("POINT   ",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                  Text(point.toString(),
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ],
              ),
            ],
          ),

          const SizedBox(
            height: 80,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text(
              randomNum1.toString(),
              style: const TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            const Text(
              " x ",
              style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            Text(
              randomNum2.toString(),
              style: const TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 0, 0, 0)),
            ),
          ]),
          // display the entered numbers
          Padding(
            padding: const EdgeInsets.all(20),
            child: SizedBox(
              height: 70,
              child: Center(
                  child: TextField(
                controller: _myController,
                textAlign: TextAlign.center,
                showCursor: false,
                style: const TextStyle(fontSize: 40),
                // Disable the default soft keybaord
                keyboardType: TextInputType.none,
              )),
            ),
          ),
          // implement the custom NumPad
          NumPad(
            buttonSize: 75,
            buttonColor: Colors.blue,
            iconColor: const Color.fromARGB(255, 34, 255, 119),
            controller: _myController,
            delete: () {
              _myController.text = _myController.text
                  .substring(0, _myController.text.length - 1);
            },
            onSubmit: () {
              setState(() {
                sum = randomNum1 * randomNum2;
                randomNum1 = random.nextInt(10);
                if (randomNum1 == 0) {
                  randomNum1 = random.nextInt(10);
                }
                randomNum2 = random.nextInt(10);
                if (randomNum2 == 0) {
                  randomNum2 = random.nextInt(10);
                }
                if (sum.toString() == _myController.text) {
                  point = point + 1;
                }
                //start timer only first round
                controller.reverse(
                    from: controller.value == 0 ? 1.0 : controller.value);
                setState(() {
                  isPlaying = true;
                  //reset sum
                });

                //clear text
                _myController.clear();
                sum = 0;
              });
            },
          ),
        ],
      ),
    );
  }
}
