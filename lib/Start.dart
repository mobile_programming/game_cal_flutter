import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'view/mode.dart';

class Start extends StatelessWidget {
  const Start({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Start',
      home: Scaffold(
        body: Main(),
      ),
    );
  }
}

class Main extends StatefulWidget {
  const Main({super.key});

  @override
  State<Main> createState() => _Main();
}

class _Main extends State<Main> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: Center(
        child: Column(children: <Widget>[
          const SizedBox(
            height: 100,
          ),
          const Image(
            image: AssetImage("img/dekBalloon.png"),
            width: 400,
            height: 450,
          ),
          const SizedBox(
            height: 35,
          ),
          const Text(
            "QUICK MATH",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          const SizedBox(
            height: 80,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size(275, 80),
              primary: const Color.fromARGB(255, 137, 205, 131),
            ),
            child: const Text(
              'Start',
              style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 249, 249, 251)),
            ),
            onPressed: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Mode()),
              ),
            },
          ),
        ]),
      ),
    );
  }
}
