// main.dart
import 'package:flutter/material.dart';
import 'package:game_cal_flutter/Level/level_divide.dart';
import 'package:game_cal_flutter/Level/level_minus.dart';
import 'package:game_cal_flutter/Level/level_multiply.dart';
import 'package:game_cal_flutter/Level/level_plus.dart';
import '../Start.dart';

class Mode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Center(
            child: Column(children: <Widget>[
              SizedBox(
                height: 40,
              ),
              Image(
                image: AssetImage("img/dekBalloon.png"),
                width: 300,
                height: 350,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                "QUICK MATH",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w900,
                    color: Colors.black),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Opacity(
                      opacity: 0.89,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(125, 120),
                          primary: Colors.lightBlueAccent,
                        ),
                        child: Text(
                          "+",
                          style: TextStyle(fontSize: 40),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LevelPlus()),
                          );
                        },
                      ),
                    ),
                  ),
                  Padding(padding: const EdgeInsets.all(10)),
                  Container(
                    child: Opacity(
                      opacity: 0.89,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(125, 120),
                          primary: Colors.lightBlueAccent,
                        ),
                        child: Text("-", style: TextStyle(fontSize: 60)),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LevelMinus()),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Opacity(
                      opacity: 0.89,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(125, 120),
                          primary: Colors.lightBlueAccent,
                        ),
                        child: Text("x", style: TextStyle(fontSize: 40)),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LevelMultiplay()),
                          );
                        },
                      ),
                    ),
                  ),
                  Padding(padding: const EdgeInsets.all(10)),
                  Container(
                    child: Opacity(
                      opacity: 0.89,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(125, 120),
                          primary: Colors.lightBlueAccent,
                        ),
                        child: Text("÷", style: TextStyle(fontSize: 45)),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LevelDivide()),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "SELECT MODE",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ]),
          ),
          Align(
            alignment: Alignment(-0.8, -0.8),
            child: Container(
              child: Opacity(
                opacity: 0.6,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(60, 60),
                    primary: Colors.black,
                  ),
                  child: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Start()),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
